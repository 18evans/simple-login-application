package com.example.loginapplication;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.loginapplication.rest.ApiManager;
import com.example.loginapplication.rest.LoginService;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Android Material library is optimized for latest Android Version hence the TextInputEditText might not work optimally on older Android versions
 */
public class LoginActivity extends AppCompatActivity {
    private final static String LOG_TAG = LoginActivity.class.getSimpleName();

    private final LoginService loginService = ApiManager.getInstance().create(LoginService.class);

    @BindView(R.id.email_edit_text)
    protected TextInputEditText emailText;
    @BindView(R.id.password_edit_text)
    protected TextInputEditText passwordText;
    @BindView(R.id.btn_login)
    protected MaterialButton btnLogin;
    @BindView(R.id.btn_cancel_clear)
    protected MaterialButton btnCancelClear;
    @BindView(R.id.progressBar)
    protected ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginactivity);
        ButterKnife.bind(this);

        btnCancelClear.setOnClickListener(v -> {
            if (isLogging) {
                isLogging = false;
                btnLogin.setVisibility(View.VISIBLE);
                btnCancelClear.setText(getString(R.string.btn_clear_login));
                progressBar.setVisibility(ProgressBar.GONE);
            } else {
                clearForm();
            }
        });
        btnLogin.setOnClickListener(v -> login());
    }


    private void clearForm() {
        emailText.setText(null);
        passwordText.setText(null);
        emailText.requestFocus();
    }

    private boolean isLogging;

    private void login() {
        Log.d(LOG_TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }
        isLogging = true;
        btnLogin.setVisibility(View.GONE);
        btnCancelClear.setText(getString(R.string.btn_cancel_login));
        btnCancelClear.setVisibility(View.VISIBLE);
//        btnLogin.setEnabled(false);
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(ProgressBar.VISIBLE);

        final String email = emailText.getText().toString();

        loginService.getRandomToken(email).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.code() != 200 || response.body() == null) {
                    String errorMessage = " Error code " + response.code() + " " + response.message() + ". Email might not exist.";
                    emailText.setError(response.message() + ". Email might not exist.");
                    onFailure(call, new Throwable(errorMessage));
                    onLoginFailed();
                    return;
                }

                String randomString = response.body();

                String password = passwordText.getText().toString();
                String hashedPassword = getSha256Hash(password);
                String salt = getRandom10CharString();
                String hashedAll = getSha1Hash(hashedPassword + randomString + salt);

                createNewToken(email, salt, hashedAll);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e(LOG_TAG, "Failed getting random string: " + t.getMessage());
            }
        });
    }

    /**
     * Disable going back to the MainActivity
     */
    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    /**
     * Redirects application user to the MainActivity along with the attached email for a greeting message.
     */
    private void onLoginSuccess(String username) {
        progressBar.setVisibility(ProgressBar.GONE);
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.putExtra("username", username);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_top_in, R.anim.slide_top_out);
    }

    private void onLoginFailed() {
        progressBar.setVisibility(ProgressBar.GONE);
        btnLogin.setEnabled(true);
    }

    /**
     * @return true if the input fields have the proper input type
     * else displays visualised error messages under the corresponding erroneous input field.
     */
    private boolean validate() {
        boolean valid = true;

        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        if (StringUtils.isBlank(email) || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailText.setError("Enter a valid email address");
            valid = false;
        } else {
            emailText.setError(null);
        }

        if (StringUtils.isBlank(password) || password.length() < 4 || password.length() > 16) {
            passwordText.setError("Password must be between 4 and 16 characters");
            valid = false;
        } else {
            passwordText.setError(null);
        }

        return valid;
    }

    private String getSha256Hash(String text) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            return DigestUtils.sha256Hex(text);
        }
        // else for under version 28

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256"); // digest() method called
            // to calculate message digest of an input
            // and return array of byte
            byte[] messageDigest = md.digest(text.getBytes());

            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);

            // Convert message digest into hex value
            String hashtext = no.toString(16);

            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }

            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getSha1Hash(String text) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            return DigestUtils.sha1Hex(text);
        }
        // else for under version 28
        try {
            MessageDigest mDigest = MessageDigest.getInstance("SHA1");
            byte[] result = mDigest.digest(text.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < result.length; i++) {
                sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
            }

            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @return a random generated 10 alpha-numeric character string
     * Used for salt (client_secret).
     */
    private String getRandom10CharString() {
        return RandomStringUtils.randomAlphanumeric(10);
    }

    /**
     * @param email     The local email String written in the email input field.
     * @param salt      Local generated random alphanumeric String (128 characters long).
     * @param hashedAll Concatenation between the hashed password, server token and salt.
     */
    private void createNewToken(String email, String salt, String hashedAll) {
        loginService.createNewToken(email, salt, hashedAll).enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                if (response.code() != 200 || response.body() == null) {
                    String errorMessage = "Error code " + response.code() + " " + response.message();
                    passwordText.setError(response.message() + ". Password is incorrect.");
                    onFailure(call, new Throwable(errorMessage));
                    onLoginFailed();
                    return;
                }

                try {
                    String createdToken = response.body().getString("created_token");
                    getUserProfile(createdToken);
                } catch (JSONException e) {
                    e.printStackTrace();
                    String errorMessage = "Error: " + response.message() + " Problem with retrieving created user token.";
                    onFailure(call, new Throwable(errorMessage));
                    Toast.makeText(LoginActivity.this, getString(R.string.error_server), Toast.LENGTH_LONG).show();
                    onLoginFailed();
                    return;
                }

            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                Log.e(LOG_TAG, "Failed creating new token: " + t.getMessage());
            }
        });
    }

    /**
     * Method retrieves user profile.
     * Called after successful authentication of create new token.
     *
     * @param createdToken the previously client posted created new token request
     */
    private void getUserProfile(String createdToken) {
        loginService.getUserProfile(createdToken).enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                if (response.code() != 200 || response.body() == null) {
                    String errorMessage = " Error code " + response.code() + " " + response.message();
                    onFailure(call, new Throwable(errorMessage));
                    onLoginFailed();
                    return;
                }
                //success
                try {
                    onLoginSuccess(response.body().getString("username"));
                    isLogging = true;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                Log.e(LOG_TAG, "Failed getting user profile: " + t.getMessage());
            }
        });
    }
}
