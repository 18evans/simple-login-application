package com.example.loginapplication.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Singleton for API connection manager.
 */
public class ApiManager {

    private static final ApiManager apiManager = new ApiManager();
    private final Retrofit retrofit;

    private ApiManager() {
        Gson gson = new GsonBuilder().serializeNulls().create();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS);

        retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl("https://localhost:9090/rest/system/")
                .client(httpClient.build())
                .build();
    }

    public static ApiManager getInstance() {
        return apiManager;
    }

    public <T> T create(Class<T> service) {
        return retrofit.create(service);
    }
}
