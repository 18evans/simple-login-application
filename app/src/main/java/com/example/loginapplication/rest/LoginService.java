package com.example.loginapplication.rest;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Local defined methods for REST-ful API requests.
 */
public interface LoginService {

    @GET("user/token")
    Call<String> getRandomToken(@Query("email") String email);

    @POST("user/token")
    @FormUrlEncoded
    Call<JSONObject> createNewToken(@Field("email") String email, @Field("clientSecret") String salt, @Field("hash") String hashAll);

    @GET("user/profile")
    Call<JSONObject> getUserProfile(String token);

}
